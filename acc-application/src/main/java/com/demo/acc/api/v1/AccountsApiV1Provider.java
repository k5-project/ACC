package com.demo.acc.api.v1;

import com.demo.acc.sdk.api.v1.api.AccountsApiV1Delegate;
import org.springframework.stereotype.Service;
import org.springframework.context.annotation.ComponentScan;
import com.demo.acc.sdk.api.v1.model.Account;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * A stub that provides implementation for the AccountsApiV1Delegate
 */
@SuppressWarnings("unused")
@Service
@ComponentScan(basePackages = "com.demo.acc.sdk.api.v1.api")
public class AccountsApiV1Provider implements AccountsApiV1Delegate {

  @Override
  public ResponseEntity<List<Account>> getAccounts() {
    //TODO Auto-generated method stub
    return ResponseEntity.ok(null);
  }

}
